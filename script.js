function initSlider() {
  $(".testimonial-wrapper").slick({
    dots: false,
    speed: 600,
    slidesToShow: 1,
    arrows: false,
  });
}

function handleChangeSlider() {
  $("a[data-slide]").click(function (e) {
    e.preventDefault();
    //Get next point
    var slideTo = $(this).data("slide") - 1;
    $(".testimonial-wrapper").slick("slickGoTo", slideTo);
    //Clear Prev Active Dot
    $(`.slider-dot:not()`).removeClass("active-dot");
    //Set Active Dot
    $(`.slider-dot:nth-child(${slideTo + 1})`).addClass("active-dot");
  });
}

$(document).ready(function () {
  const body = document.body;
  const header = document.querySelector('header')
  
  initSlider();
  handleChangeSlider();

  /* POPUP SCRIPTS */

  const overlay = document.querySelector(".overlay");
  const closePopupBtn = document.querySelectorAll(".close-popup");
  const openPopupBtn = document.querySelectorAll(".open-popup");
  const popup = document.querySelector(".pop-up-wrapper");

  const openPopup = () => {
    overlay.classList.add("active");
    body.classList.add("overflow");
    popup.classList.add("active");
  };

  const closePopup = () => {
    overlay.classList.remove("active");
    body.classList.remove("overflow");
    popup.classList.remove("active");
  };

  openPopupBtn.forEach((item) => {
    item.addEventListener("click", openPopup);
  });

  [overlay, ...closePopupBtn].forEach((item) => {
    item.addEventListener("click", closePopup);
  });

  /* SET MIN HEIGHT TO ELEMENTS */

  const setMinHeight = (parent, children) => {
    const elements = document.querySelector(parent).querySelectorAll(children);

    const minHeight = Array.from(elements)
      .map((item) => {
        const itemMinHeight = item.getBoundingClientRect();
        return itemMinHeight.height;
      })
      .sort((a, b) => b - a)[0];

    elements.forEach((item) => (item.style.minHeight = `${minHeight}px`));
  };

  setMinHeight(".list-wrapper", "h2");


  /* CHANGE BODY COLOR ON SCROLL */

  const listenChangeColor = () => {

    const sections = document.querySelectorAll('[data-color]');
    const offsetSections = Array.from(sections).map( item => {
      return {
        color: item.getAttribute('data-color'),
        offset: item.offsetTop
      }
    })

    window.addEventListener('scroll', () => {

      const setColor = offsetSections.filter(item => window.scrollY >= item.offset).at(-1).color
      const classListClear = ['blue', 'white', 'dark'].filter(item => item !== setColor)
      header.classList.remove(...classListClear)
      !header.classList.contains(setColor) && header.classList.add(setColor)

    })

  }

  listenChangeColor()

});
